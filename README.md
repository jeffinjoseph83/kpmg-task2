# kpmg-task2


## challenge

We need to write code that will query the meta data of an instance within AWS or Azure or GCP and provide a json formatted output. 

## sample output

`{
    "meta-data": {
        "ami-id": "ami-0b5eea76982371e91",
        "ami-launch-index": 0,
        "ami-manifest-path": "(unknown)",
        "block-device-mapping": {
            "ami": "/dev/xvda",
            "root": "/dev/xvda"
        },
        "events": {
            "maintenance": {
                "history": [],
                "scheduled": []
            }
        },
        "hostname": "ip-172-31-54-121.ec2.internal",
        "identity-credentials": {
            "ec2": {
                "info": {
                    "AccountId": "434022486821",
                    "Code": "Success",
                    "LastUpdated": "2023-01-19T04:54:06Z"
                },
                "security-credentials": {
                    "ec2-instance": {
                        "AccessKeyId": "ASIAWKDOBSMS4TK5KSPY",
                        "Code": "Success",
                        "Expiration": "2023-01-19T11:22:18Z",
                        "LastUpdated": "2023-01-19T04:54:05Z",
                        "SecretAccessKey": "PZtmj4otSt+b6rOrSJUuAjQ0ft2tDomSNuuVtEJK",
                        "Token": "IQoJb3JpZ2luX2VjEAUaCXVzLWVhc3QtMSJHMEUCIBqKOPC887d4yZ6+kQ4Tncti+WrkKiFwYLEj//J4oiDbAiEA74uOKopeOdRtn7La0ZI7DJDcAJ9iXf9RmzLOMddt68kqxgQIbhAAGgw0MzQwMjI0ODY4MjEiDAYrgo1mCS8uOVEu/SqjBGfDDcGp898Prh9ncc+EoRMf6Ot2dGaGaqBWfrf+Wwx3AF2fKUYe7Gfdhtu9oDwRQiBvnabj7osjXG2pWqACi+GOZ3rmNT5Xq/HuQachiCwh+PIsXV0oT7Biomn+mj5DIlcRXV81q+IlEZpZjufsmj3O3SF1nXADsXNJUJpxtTGgjOnqDPbCxbhaxu0fWyqnVPWs3KpWxkRywmwaPURTqseUP3aGrydiVByerJmffZUaSLj+nDBwquxKCAhldJ5wZmNQKQm1GA1IrZ83AjFNX32/l/yKFbMDeEz3Jq0d9eJLgH/ZwWPt4dMoWWSmKoQ0Dt47a75MUIikgs9W1obeeIjlkY/m/J6C9ijo0sbs0Fa+2ZEtqbP3/mJDnOQ8xssTn6SiOkm7+89ozr76t4nx2HUsV95QYYheNEdXosYaT1/VbfuwBKNUFzyLVA/4QD6RqpYeaqp15r6VCCYAMyYRZ7nQHkpcv/VhHnhENFjbvrggAjc0/8xg3+o+eRBTSfxpWe5qW5yFSYSdgrcohFJLgPhEp9Iyo5lyJxg/6ja3ZiX0nLJIozYRxRm+CrepbPhqojVM0IG2kE5twd7d2Bi97RkS7jK0+100BguXwHHa9k+ZJP9I/cccap7PL2a1cILh8WdEGbKCsgKscfxcUe9feyTGREKFmCmxasOhS0EYg8ECmJPiDlOY1kXvLnJaw1VD3OwW4ME+H6NKbWYbU2Yxhe2vwGowkZqjngY6kwK6RZbFK27fyEQg5odxkpxhGd3X+YXBnfxv7NEy0lG1/h5xIr8tmyAU5hEQbjdsyk/VOHdOLwktcol8Nfp+PZPy+9UDgGYLDsqRyhitUxE4s8AgX2NmwZP86likIPemCINfEl4fgHAXYvfpHmEgQCBn/XUiDKZVUqP4gmwQJYetdA9V4cmSZ7CnzXTZ41HMoh99ToJ3g++0WXGx2PQ4aqfDtcZIc0zM56uMgDyccXdL0aX0mritoEP9TjLhzus8sXhvsRwhXJ+Clwmn+WlnM+i1rtyWyLMqeNmce9VBP031CpAxvr6GG/Xd+/SNFKJzmO/onKiqkN1lNggRRqJISG9T1l3Uyd+Vb8JeQ31pROfwdw859w==",
                        "Type": "AWS-HMAC"
                    }
                }
            }
        },
        "instance-action": "none",
        "instance-id": "i-0cb800318a03c038e",
        "instance-life-cycle": "on-demand",
        "instance-type": "t2.micro",
        "local-hostname": "ip-172-31-54-121.ec2.internal",
        "local-ipv4": "172.31.54.121",
        "mac": "06:39:83:20:9d:7b",
        "managed-ssh-keys": {
            "signer-cert": "-----BEGIN CERTIFICATE-----\nMIIGCjCCBPKgAwIBAgIQCXe9Xsdb83czu9oK1cWMZzANBgkqhkiG9w0BAQsFADBG\nMQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRUwEwYDVQQLEwxTZXJ2ZXIg\nQ0EgMUIxDzANBgNVBAMTBkFtYXpvbjAeFw0yMjA5MjEwMDAwMDBaFw0yMzA5MTEy\nMzU5NTlaMDUxMzAxBgNVBAMTKm1hbmFnZWQtc3NoLXNpZ25lci51cy1lYXN0LTEu\nYW1hem9uYXdzLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMQe\nvyv/NrtuDuIkovmR3AOgXlMziLI4OMkX8TKlPW7vKp5OBm0edHZ3jEqVPpCqPVNA\nT5P9vpiWHRkudXhabg+v9Bu5h8Ek6KQFMv7IhCCmrNxVp09Yc5IacRk2byzCnYXB\n57ICarPvmRiN9yZJco0LIq4Uag6Y4gqmuedsZfoKkyOGO6kU771XDxUp2+d3wzdg\nGYfpX7EIMWAvH6PBiKjH1rWHohiRmTRbJDysZuva+kSxcUdS5pzWSRR+fTV1DXDT\nXqoavqy3o+zuhulIHLjgeo2cvSHB6v7Ei9g4nQ5SChgc95KiOZPAjHh35yO2ewnw\nHuJTunDlgI4gIM5/xG0CAwEAAaOCAwMwggL/MB8GA1UdIwQYMBaAFFmkZgZSoHuV\nkjyjlAcnlnRb+T3QMB0GA1UdDgQWBBQkGv3vIyT1Gj4PdZuBm1TD8LDA0zA1BgNV\nHREELjAsgiptYW5hZ2VkLXNzaC1zaWduZXIudXMtZWFzdC0xLmFtYXpvbmF3cy5j\nb20wDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcD\nAjA9BgNVHR8ENjA0MDKgMKAuhixodHRwOi8vY3JsLnNjYTFiLmFtYXpvbnRydXN0\nLmNvbS9zY2ExYi0xLmNybDATBgNVHSAEDDAKMAgGBmeBDAECATB1BggrBgEFBQcB\nAQRpMGcwLQYIKwYBBQUHMAGGIWh0dHA6Ly9vY3NwLnNjYTFiLmFtYXpvbnRydXN0\nLmNvbTA2BggrBgEFBQcwAoYqaHR0cDovL2NydC5zY2ExYi5hbWF6b250cnVzdC5j\nb20vc2NhMWIuY3J0MAwGA1UdEwEB/wQCMAAwggF8BgorBgEEAdZ5AgQCBIIBbASC\nAWgBZgB1AK33vvp8/xDIi509nB4+GGq0Zyldz7EMJMqFhjTr3IKKAAABg19ldx8A\nAAQDAEYwRAIgL21i/PzgW2zLWe3soLKDHRM3yY2VAJNWajA/LGPU6/QCIHAk6dVF\nhx0iwclF5PNDgVKuCLJG5wusimheyR8/enKdAHYANc8ZG7+xbFe/D61MbULLu7Yn\nICZR6j/hKu+oA8M71kwAAAGDX2V3DQAABAMARzBFAiBh2W6V+YJDt8vYNHI4b4v1\nOEW1jZl9EFwD0wV2vNCpcQIhALFw4NyMdHp2N/pNxQ2pSbLu2b+Xwf9+FoF1/YBJ\n+uSHAHUAs3N3B+GEUPhjhtYFqdwRCUp5LbFnDAuH3PADDnk2pZoAAAGDX2V3SgAA\nBAMARjBEAiANzSGaHPhio4pl2dyOwBh8/ya+1Sia7UINoEfT2MJFHQIgPuzB7G8p\nE8kMXNCKep+v3drr3I+1MW+aKsV4p80S/i8wDQYJKoZIhvcNAQELBQADggEBALW1\n971aiGPiOi/qPazU7kAzfI48rFW9Wnllgasr7abgos8mOcksNZEMDZ2353XVc618\nK459OTjHoJz1WqKDtovykYAMYJw3muksBOUF1byeb/D0CK3E6310yeo7H0Bpyx4b\nYewwScILdHMHOd348wzIz+yTwugrY9tXk4f8gklkDtZAtGx1euG/602HZUP3S/Z9\nNSEt4vLMSiYYQ3GYZVYugFeNYN5bhdq7kE83jXQL98zi4iScc964zMC0/1pGLw/4\n2rQpI9tS8nd6qN2t4GyMv2s1Z47r0P+o93ugwQb319Nlxox0Bzm2zYp2z++71f22\nQ5gd3CoKDjLEMTQ4n8Y=\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIESTCCAzGgAwIBAgITBn+UV4WH6Kx33rJTMlu8mYtWDTANBgkqhkiG9w0BAQsF\nADA5MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRkwFwYDVQQDExBBbWF6\nb24gUm9vdCBDQSAxMB4XDTE1MTAyMjAwMDAwMFoXDTI1MTAxOTAwMDAwMFowRjEL\nMAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEVMBMGA1UECxMMU2VydmVyIENB\nIDFCMQ8wDQYDVQQDEwZBbWF6b24wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK\nAoIBAQDCThZn3c68asg3Wuw6MLAd5tES6BIoSMzoKcG5blPVo+sDORrMd4f2AbnZ\ncMzPa43j4wNxhplty6aUKk4T1qe9BOwKFjwK6zmxxLVYo7bHViXsPlJ6qOMpFge5\nblDP+18x+B26A0piiQOuPkfyDyeR4xQghfj66Yo19V+emU3nazfvpFA+ROz6WoVm\nB5x+F2pV8xeKNR7u6azDdU5YVX1TawprmxRC1+WsAYmz6qP+z8ArDITC2FMVy2fw\n0IjKOtEXc/VfmtTFch5+AfGYMGMqqvJ6LcXiAhqG5TI+Dr0RtM88k+8XUBCeQ8IG\nKuANaL7TiItKZYxK1MMuTJtV9IblAgMBAAGjggE7MIIBNzASBgNVHRMBAf8ECDAG\nAQH/AgEAMA4GA1UdDwEB/wQEAwIBhjAdBgNVHQ4EFgQUWaRmBlKge5WSPKOUByeW\ndFv5PdAwHwYDVR0jBBgwFoAUhBjMhTTsvAyUlC4IWZzHshBOCggwewYIKwYBBQUH\nAQEEbzBtMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5yb290Y2ExLmFtYXpvbnRy\ndXN0LmNvbTA6BggrBgEFBQcwAoYuaHR0cDovL2NydC5yb290Y2ExLmFtYXpvbnRy\ndXN0LmNvbS9yb290Y2ExLmNlcjA/BgNVHR8EODA2MDSgMqAwhi5odHRwOi8vY3Js\nLnJvb3RjYTEuYW1hem9udHJ1c3QuY29tL3Jvb3RjYTEuY3JsMBMGA1UdIAQMMAow\nCAYGZ4EMAQIBMA0GCSqGSIb3DQEBCwUAA4IBAQCFkr41u3nPo4FCHOTjY3NTOVI1\n59Gt/a6ZiqyJEi+752+a1U5y6iAwYfmXss2lJwJFqMp2PphKg5625kXg8kP2CN5t\n6G7bMQcT8C8xDZNtYTd7WPD8UZiRKAJPBXa30/AbwuZe0GaFEQ8ugcYQgSn+IGBI\n8/LwhBNTZTUVEWuCUUBVV18YtbAiPq3yXqMB48Oz+ctBWuZSkbvkNodPLamkB2g1\nupRyzQ7qDn1X8nn8N8V7YJ6y68AtkHcNSRAnpTitxBKjtKPISLMVCx7i4hncxHZS\nyLyKQXhw2W2Xs0qLeC1etA+jTGDK4UfLeC0SF7FSi8o5LL21L8IzApar2pR/\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIEkjCCA3qgAwIBAgITBn+USionzfP6wq4rAfkI7rnExjANBgkqhkiG9w0BAQsF\nADCBmDELMAkGA1UEBhMCVVMxEDAOBgNVBAgTB0FyaXpvbmExEzARBgNVBAcTClNj\nb3R0c2RhbGUxJTAjBgNVBAoTHFN0YXJmaWVsZCBUZWNobm9sb2dpZXMsIEluYy4x\nOzA5BgNVBAMTMlN0YXJmaWVsZCBTZXJ2aWNlcyBSb290IENlcnRpZmljYXRlIEF1\ndGhvcml0eSAtIEcyMB4XDTE1MDUyNTEyMDAwMFoXDTM3MTIzMTAxMDAwMFowOTEL\nMAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEZMBcGA1UEAxMQQW1hem9uIFJv\nb3QgQ0EgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJ4gHHKeNXj\nca9HgFB0fW7Y14h29Jlo91ghYPl0hAEvrAIthtOgQ3pOsqTQNroBvo3bSMgHFzZM\n9O6II8c+6zf1tRn4SWiw3te5djgdYZ6k/oI2peVKVuRF4fn9tBb6dNqcmzU5L/qw\nIFAGbHrQgLKm+a/sRxmPUDgH3KKHOVj4utWp+UhnMJbulHheb4mjUcAwhmahRWa6\nVOujw5H5SNz/0egwLX0tdHA114gk957EWW67c4cX8jJGKLhD+rcdqsq08p8kDi1L\n93FcXmn/6pUCyziKrlA4b9v7LWIbxcceVOF34GfID5yHI9Y/QCB/IIDEgEw+OyQm\njgSubJrIqg0CAwEAAaOCATEwggEtMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/\nBAQDAgGGMB0GA1UdDgQWBBSEGMyFNOy8DJSULghZnMeyEE4KCDAfBgNVHSMEGDAW\ngBScXwDfqgHXMCs4iKK4bUqc8hGRgzB4BggrBgEFBQcBAQRsMGowLgYIKwYBBQUH\nMAGGImh0dHA6Ly9vY3NwLnJvb3RnMi5hbWF6b250cnVzdC5jb20wOAYIKwYBBQUH\nMAKGLGh0dHA6Ly9jcnQucm9vdGcyLmFtYXpvbnRydXN0LmNvbS9yb290ZzIuY2Vy\nMD0GA1UdHwQ2MDQwMqAwoC6GLGh0dHA6Ly9jcmwucm9vdGcyLmFtYXpvbnRydXN0\nLmNvbS9yb290ZzIuY3JsMBEGA1UdIAQKMAgwBgYEVR0gADANBgkqhkiG9w0BAQsF\nAAOCAQEAYjdCXLwQtT6LLOkMm2xF4gcAevnFWAu5CIw+7bMlPLVvUOTNNWqnkzSW\nMiGpSESrnO09tKpzbeR/FoCJbM8oAxiDR3mjEH4wW6w7sGDgd9QIpuEdfF7Au/ma\neyKdpwAJfqxGF4PcnCZXmTA5YpaP7dreqsXMGz7KQ2hsVxa81Q4gLv7/wmpdLqBK\nbRRYh5TmOTFffHPLkIhqhBGWJ6bt2YFGpn6jcgAKUj6DiAdjd4lpFw85hdKrCEVN\n0FE6/V1dN2RMfjCyVSRCnTawXZwXgWHxyvkQAiSr6w10kY17RSlQOYiypok1JR4U\nakcjMS9cmvqtmg5iUaQqqcT5NJ0hGA==\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIEdTCCA12gAwIBAgIJAKcOSkw0grd/MA0GCSqGSIb3DQEBCwUAMGgxCzAJBgNV\nBAYTAlVTMSUwIwYDVQQKExxTdGFyZmllbGQgVGVjaG5vbG9naWVzLCBJbmMuMTIw\nMAYDVQQLEylTdGFyZmllbGQgQ2xhc3MgMiBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0\neTAeFw0wOTA5MDIwMDAwMDBaFw0zNDA2MjgxNzM5MTZaMIGYMQswCQYDVQQGEwJV\nUzEQMA4GA1UECBMHQXJpem9uYTETMBEGA1UEBxMKU2NvdHRzZGFsZTElMCMGA1UE\nChMcU3RhcmZpZWxkIFRlY2hub2xvZ2llcywgSW5jLjE7MDkGA1UEAxMyU3RhcmZp\nZWxkIFNlcnZpY2VzIFJvb3QgQ2VydGlmaWNhdGUgQXV0aG9yaXR5IC0gRzIwggEi\nMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDVDDrEKvlO4vW+GZdfjohTsR8/\ny8+fIBNtKTrID30892t2OGPZNmCom15cAICyL1l/9of5JUOG52kbUpqQ4XHj2C0N\nTm/2yEnZtvMaVq4rtnQU68/7JuMauh2WLmo7WJSJR1b/JaCTcFOD2oR0FMNnngRo\nOt+OQFodSk7PQ5E751bWAHDLUu57fa4657wx+UX2wmDPE1kCK4DMNEffud6QZW0C\nzyyRpqbn3oUYSXxmTqM6bam17jQuug0DuDPfR+uxa40l2ZvOgdFFRjKWcIfeAg5J\nQ4W2bHO7ZOphQazJ1FTfhy/HIrImzJ9ZVGif/L4qL8RVHHVAYBeFAlU5i38FAgMB\nAAGjgfAwge0wDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAYYwHQYDVR0O\nBBYEFJxfAN+qAdcwKziIorhtSpzyEZGDMB8GA1UdIwQYMBaAFL9ft9HO3R+G9FtV\nrNzXEMIOqYjnME8GCCsGAQUFBwEBBEMwQTAcBggrBgEFBQcwAYYQaHR0cDovL28u\nc3MyLnVzLzAhBggrBgEFBQcwAoYVaHR0cDovL3guc3MyLnVzL3guY2VyMCYGA1Ud\nHwQfMB0wG6AZoBeGFWh0dHA6Ly9zLnNzMi51cy9yLmNybDARBgNVHSAECjAIMAYG\nBFUdIAAwDQYJKoZIhvcNAQELBQADggEBACMd44pXyn3pF3lM8R5V/cxTbj5HD9/G\nVfKyBDbtgB9TxF00KGu+x1X8Z+rLP3+QsjPNG1gQggL4+C/1E2DUBc7xgQjB3ad1\nl08YuW3e95ORCLp+QCztweq7dp4zBncdDQh/U90bZKuCJ/Fp1U1ervShw3WnWEQt\n8jxwmKy6abaVd38PMV4s/KCHOkdp8Hlf9BRUpJVeEXgSYCfOn8J3/yNTd126/+pZ\n59vPr5KW7ySaNRB6nJHGDn2Z9j8Z3/VyVOEVqQdZe4O/Ui5GjLIAZHYcSNPYeehu\nVsyuLAOQ1xk4meTKCRlb/weWsKh/NEnfVqn3sF/tM+2MR7cwA130A4w=\n-----END CERTIFICATE-----\n",
            "signer-ocsp": {
                "06B25927C42A721631C1EFD9431E648FA62E1E39": "MIIGCAoBAKCCBgEwggX9BgkrBgEFBQcwAQEEggXuMIIF6jCB4aFWMFQxCzAJBgNVBAYTAlVTMQ8wDQYDVQQKEwZBbWF6b24xNDAyBgNVBAMTK1N0YXJmaWVsZCBTZXJ2aWNlcyBHMiBSb290IENBIFN0YXR1cyBTaWduZXIYDzIwMjMwMTE3MDY1OTQyWjB2MHQwTDAJBgUrDgMCGgUABBSIfaREXmfqfJR3TkMYnD7O5MhzEgQUnF8A36oB1zArOIiiuG1KnPIRkYMCEwZ/lEoqJ83z+sKuKwH5CO65xMaAABgPMjAyMzAxMTcwNTU5NDJaoBEYDzIwMjMwMTI0MDY1OTQyWjANBgkqhkiG9w0BAQsFAAOCAQEAwrNXr/pvmKS3vNfXs2dkl5CVg9lnvM7w+C9XjYzoL6raf8niZ6lSWrgU7Zi/5uh07SbLrN6bFMjW8Nvrfg2jONqb0bJk9Xxdv/HxSRoXcQ4K7nt5kkZCz3tkc3gCyFhb/bhdBgniOrIiKBPDWvjt9pE1HPo2pTUKue+wIUY97MAgFgtDxcu1iN2gdjQ2DzSV4NtOJLYOAMPwrXo6LaJoB/QpujopJvWaX5uBUHBcJs/PF6zjX8OvY8GrhCqVgeG6XdZiYs2Rcc1s5wU8idLuszcJqJAB5OoQBbMCvCkj42AtDUpOifjPBmgurK5FiqDidSN7Al97M7LVZFsu/HvWRaCCA+4wggPqMIID5jCCAs6gAwIBAgITB2w3g2V6fdIfqwRcKjuX68017jANBgkqhkiG9w0BAQsFADCBmDELMAkGA1UEBhMCVVMxEDAOBgNVBAgTB0FyaXpvbmExEzARBgNVBAcTClNjb3R0c2RhbGUxJTAjBgNVBAoTHFN0YXJmaWVsZCBUZWNobm9sb2dpZXMsIEluYy4xOzA5BgNVBAMTMlN0YXJmaWVsZCBTZXJ2aWNlcyBSb290IENlcnRpZmljYXRlIEF1dGhvcml0eSAtIEcyMB4XDTIyMDYxNTE5NDUzNFoXDTI1MDkxNTE5NDUzNFowVDELMAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjE0MDIGA1UEAxMrU3RhcmZpZWxkIFNlcnZpY2VzIEcyIFJvb3QgQ0EgU3RhdHVzIFNpZ25lcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMPIuVO8p2cQPtjracabwCGA4surcyi66T4NGOO+Aw9IT76H3UQzneJFlQuGPPOvu8tTJsxOlntadWDYG39HTRkscDnD9WEf2EROxSZhdugRuwva00XAQgyTGoB4hfM5nEz4UK5ixCirBH4mfNHKMwYmFocrVuVVKtxcaemMsOw6i0E/ORw1xTkxIZyQQG7hfDpfB8Nr9JlhSvfCXOPRRxhbFBJhfKXa+qraIl2WywYEFxmQbTixvEid/v07xcHxYF655v+KavP2SHz282Ls/ZvSYOcpQ2Y8xSouzBZgB3TxEyOnng8LOGYDLqJXd5p1G5vmJ6yOLlNh6tfjnrNVaEECAwEAAaNsMGowDgYDVR0PAQH/BAQDAgeAMB8GA1UdIwQYMBaAFJxfAN+qAdcwKziIorhtSpzyEZGDMBMGA1UdJQQMMAoGCCsGAQUFBwMJMA8GCSsGAQUFBzABBQQCBQAwEQYDVR0gBAowCDAGBgQrgTsBMA0GCSqGSIb3DQEBCwUAA4IBAQDC4YCxxiaYtcJF8rGsm+A3q4r6FZrEQ2mUfEmumhPHJ9rzDjdGZDdrmQrLhE0sFeyyodEkVWFKPfcezcYVqWz70TTfi/uCBIhUqB+k9PfQl7DsVJuV4R1zydE+Qglp+KcGiUJ+DFla+/vwKqSDBvMKDZITa9EkdGrzyh5Q0iDsJeTHn4m/0dfWzHPZ1UAvBZTWZBw40903jwjmAVNtL3mNDnpcBu22brfs3oTlMxQ1D3f4ShCiN6ZIVGJBMUQTNNG7AuhLUH182habWd7Jm+Vp19B82KXD0cWqVUhNYC/+2q1sVU3ev7TlvBz6DMTqJR4l+RqgM0+YH2Mt4sL5oub/",
                "917E732D330F9A12404F73D8BEA36948B929DFFC": "MIIFjgoBAKCCBYcwggWDBgkrBgEFBQcwAQEEggV0MIIFcDCB1KFJMEcxCzAJBgNVBAYTAlVTMQ8wDQYDVQQKEwZBbWF6b24xJzAlBgNVBAMTHkFtYXpvbiBSb290IENBIDEgU3RhdHVzIFNpZ25lchgPMjAyMzAxMTUyMDA3MzVaMHYwdDBMMAkGBSsOAwIaBQAEFE9Zo5RTz7lVnn9r2MVNpT2mQrcUBBSEGMyFNOy8DJSULghZnMeyEE4KCAITBn+UV4WH6Kx33rJTMlu8mYtWDYAAGA8yMDIzMDExNTE5MDczNVqgERgPMjAyMzAxMjIyMDA3MzVaMA0GCSqGSIb3DQEBCwUAA4IBAQB2GAywafXFpeDnk+n/9cIQRIAPmB7kqpvwv79uxZ5kjTiTZoJeyexm17VW06uSfJ1XSGy7OEqzaRgpSaahYbZ1oad1tDGmuCu/u27oxm4LW/0S5IvsZd2cxYqpMTszxXODcy/xcMKVT6WaCsUUQVIhX+pFkfFlbYZ4MvPhP9qpOYqfJF4P5tvaC3dxZXiy8XeN584V8UQUNfGIg3rrOK0/hQHrGb7FFQTdCP2glBcc+ae9bosWQjwkxhO5FbykI6kTCat18PYkocRdZQxnu2vcFCd/ytussTAy4oDJvAcjAMgFlsNuoCDsEn113soewu6HlMZK0oEuIiLlKG10MKZPoIIDgTCCA30wggN5MIICYaADAgECAhMHbDdvQwH2qE5cnWNHH2aNLOfNMA0GCSqGSIb3DQEBCwUAMDkxCzAJBgNVBAYTAlVTMQ8wDQYDVQQKEwZBbWF6b24xGTAXBgNVBAMTEEFtYXpvbiBSb290IENBIDEwHhcNMjIwNjE1MTk0MTA2WhcNMjUwOTE1MTk0MTA2WjBHMQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMScwJQYDVQQDEx5BbWF6b24gUm9vdCBDQSAxIFN0YXR1cyBTaWduZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC2rzR/MEH7mKG98/oRwQbSXeaeBsmJ1CnelLG1ZFd+BqArQuyAZm/RtlRQtCJsULnRw5OEMyyYkN8EnoRJPu6/6D2oG775llBogBmAOX7Ia/WWL9g/8uikOv3dFxJAkO1x2zEApTPaXzPdN0Bgf6pZxPUc5Lzpu6VcPfbI2KkcroVHkgPuRwA5Zd7hZ3yuHSv2QytW7QKC9n81BnjMiAUUwhNPM60LXhdua6HlsO7s5W2MtYz86YMv0Ad0KEnve/uqTdMKmiLpdUNR0C1zW1oqN3hohuq6vHbkbUeLJs0FRAToS691DvgBtboqC5Uo3JX2zv4AoZLh6RmTaEyV2JlvAgMBAAGjbDBqMA4GA1UdDwEB/wQEAwIHgDAfBgNVHSMEGDAWgBSEGMyFNOy8DJSULghZnMeyEE4KCDATBgNVHSUEDDAKBggrBgEFBQcDCTAPBgkrBgEFBQcwAQUEAgUAMBEGA1UdIAQKMAgwBgYEK4E7ATANBgkqhkiG9w0BAQsFAAOCAQEACqgXJeWfX2Xhp6mYM/JKCo3EF1LEb3nny3i4OaIK+zvLHMwjt+yUoeGQK90AwBoGF5juf95LRqxTUfY7FyBa9GfSDTUK+XmSxm/G3CLPtaJWSzhLpw08xTfOwalz7lpT+u09/ltfUYKhpppFdLkkhTA57X8AgDAkxOtSLC97G98yH9acJkQNkNabEmMCRNUg501aZBtcirHYM9n/McOqhK3dyw0c7LDSYOFRk1EAnUvZBwCY7z1Jk42zupX/9pnSk5zI0ZUq8EhU4/n2ioBJeLMQtQlYUFXnbdYC4g9R5Qp953rSsnlCiVqkEfXTy0Cbx7Mv6GVQ5e/kRfzR7Aps4w==",
                "9E99A48A9960B14926BB7F3B02E22DA2B0AB7280": "MIIGzQoBAKCCBsYwggbCBgkrBgEFBQcwAQEEggazMIIGrzCCAROhgZEwgY4xCzAJBgNVBAYTAlVTMRAwDgYDVQQIEwdBcml6b25hMRMwEQYDVQQHEwpTY290dHNkYWxlMSUwIwYDVQQKExxTdGFyZmllbGQgVGVjaG5vbG9naWVzLCBJbmMuMTEwLwYDVQQDEyhTdGFyZmllbGQgUm9vdCBWYWxpZGF0aW9uIEF1dGhvcml0eSAtIEcxGA8yMDIzMDExOTAxMjIzMFowbDBqMEIwCQYFKw4DAhoFAAQUi8GehFuYHWHPVGkhGmi44xEzbZAEFL9ft9HO3R+G9FtVrNzXEMIOqYjnAgkApw5KTDSCt3+AABgPMjAyMzAxMTkwMTIyMzBaoBEYDzIwMjMwMTIwMTMyMjMwWjANBgkqhkiG9w0BAQsFAAOCAQEAh2x8E2EQExqCR/U3Qvt/ijji11I2ozL5dNV8FrAABCpQ+zUu7D4xc5LVbPyrTHowklIFncoomZiDASAy4UYRIXRjpQHtRhhsO47/6xZtQjFiJeIqlNs8WSJ4r2hV+nXyJsnYeB8JoPwCBL0Ns2tb8nbdVGMhCFFaiPVJ3kiWucmxullEEqcoS/2Ek1GWN/5YmSPChTyavdLVRH/MZkGwjkn+y5XvwiNyF1Wb2HIBv2YoD4g+fBbgaCQUMXz/HkSKI6mOFoEf8t40DmM9ETR9rTZvjQETFMQRrWsKDqw2d0Tm0eOQgPRMMxJZ++GDxdc/c0A+sbnxi7XRXE6ecYxeEaCCBIAwggR8MIIEeDCCA2CgAwIBAgIJAMWm2zaRGjYpMA0GCSqGSIb3DQEBCwUAMGgxCzAJBgNVBAYTAlVTMSUwIwYDVQQKExxTdGFyZmllbGQgVGVjaG5vbG9naWVzLCBJbmMuMTIwMAYDVQQLEylTdGFyZmllbGQgQ2xhc3MgMiBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0yMjA2MjIwNzAwMDBaFw0yMzA2MjIwNzAwMDBaMIGOMQswCQYDVQQGEwJVUzEQMA4GA1UECBMHQXJpem9uYTETMBEGA1UEBxMKU2NvdHRzZGFsZTElMCMGA1UEChMcU3RhcmZpZWxkIFRlY2hub2xvZ2llcywgSW5jLjExMC8GA1UEAxMoU3RhcmZpZWxkIFJvb3QgVmFsaWRhdGlvbiBBdXRob3JpdHkgLSBHMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALu/HuOgdrP3cqmJXeiMVpfqTT7tjLGZUqNUNX6I1MwZBrQjOihDNVJ7Jz6W79+wtkiWbjX6Ojr5r8P86THX06bEhtcV0O6GEVLikJlytJcwrYZQ+zjNJWxPz36p6knT87gzWh7FFmoz2J/nNqlWxZXEfMsridbAQ7fuOVuHDc7sxcBOGjO7k0xr870vUAtjDILHR0v8ThylW4P4xpgJ9d9Y6gd8F4wuLiZUIngkONiTJO7yQ9BBbi4d7sIm5MgVHzp0Nlbg508VyiSRF4yg6dt4lXwPeOkklAAJvQrJhjsDXzhtlmoP0wfW7k1NPWl3fe5ncBuO25xTnXtcNglvSAUCAwEAAaOB/TCB+jAMBgNVHRMBAf8EAjAAMA4GA1UdDwEB/wQEAwIGwDATBgNVHSUEDDAKBggrBgEFBQcDCTAdBgNVHQ4EFgQUfeph7TGiQSqyHbxuRd2FOm4HfR8wDwYJKwYBBQUHMAEFBAIFADBDBgNVHR8EPDA6MDigNqA0hjJodHRwOi8vY3JsLnN0YXJmaWVsZHRlY2guY29tL3JlcG9zaXRvcnkvc2Zyb290LmNybDBQBgNVHSAESTBHMEUGC2CGSAGG/W4BBxcBMDYwNAYIKwYBBQUHAgEWKGh0dHA6Ly9jcmwuc3RhcmZpZWxkdGVjaC5jb20vcmVwb3NpdG9yeS8wDQYJKoZIhvcNAQELBQADggEBAKrhOg9GbcDAosjhLCjDq5GAuvnKbw+QglV/g/EYn89IepeDfpK9n0tY2zSnVyYnhxMIHY+H4V084OCtbmJxmHFk/QTO1oH1tC4Wv05RuxdCx4IHfgCaeXhkK/FRsekoi2DTN0CVJ558j8FGOD+cqPxBwdYXR6eguFFDZ3hFw3R5GVBq925f2tIQoizVlYVWp7zmor3SwzER3sghIaoZ6hbhtSknLlAiGjNjhcBXcAhq7nyGecFryEJ64jQoZQCDEEvgTWU3kwW6i0Yru4KyM1ztkxfqyN3P5AHSycOC+KJ4JhiYhMgbpUBFAZnl/298YeaglUhF6aLooGmOPnp1Txo=",
                "BD9395854C75C928F5109143FEBEDF9655440EE0": "MIIB0woBAKCCAcwwggHIBgkrBgEFBQcwAQEEggG5MIIBtTCBnqIWBBRZpGYGUqB7lZI8o5QHJ5Z0W/k90BgPMjAyMzAxMTgyMDMwNTFaMHMwcTBJMAkGBSsOAwIaBQAEFDP1qsYdZucFXQMXOk0fPhhxOIUNBBRZpGYGUqB7lZI8o5QHJ5Z0W/k90AIQCXe9Xsdb83czu9oK1cWMZ4AAGA8yMDIzMDExODIwMTUwMlqgERgPMjAyMzAxMjUxOTMwMDJaMA0GCSqGSIb3DQEBCwUAA4IBAQB5LR8tlwBAnGDgtzqoCKrP3FwaMc2+tOBnKbiI9abAfciAgZZKLr5y0+gQpbObrK9qQOEml3xk4g+J6eMmYxG1qjWFUYhvEXbGItS+NWkM8rPLafocNKvpan0K600YXQOdrfojxGNSeU3HgMuFSTHQIx71wtWQx7rY+Q7sc/QCUe+puEQ6bNJhZP2jsyGjklBXkiWBaF5bcNo45CP9YeE+0GiLZFWkcmL/Sa3vpgtuvNSG2ye05FY6Y75FTv+MNaHY/rwBHodkCWrBCOcDKTS5pL+b0i9EAic+QH5CsS/BvAjZPBvedI6BAVLQ3w1tqhR51cVsSHrwH5qF0e6UxjPy"
            }
        },
        "metrics": {
            "vhostmd": "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
        },
        "network": {
            "interfaces": {
                "macs": {
                    "06:39:83:20:9d:7b": {
                        "device-number": 0,
                        "interface-id": "eni-075128597deee2de5",
                        "ipv4-associations": {
                            "100.26.169.136": "172.31.54.121"
                        },
                        "local-hostname": "ip-172-31-54-121.ec2.internal",
                        "local-ipv4s": "172.31.54.121",
                        "mac": "06:39:83:20:9d:7b",
                        "owner-id": 434022486821,
                        "public-hostname": "ec2-100-26-169-136.compute-1.amazonaws.com",
                        "public-ipv4s": "100.26.169.136",
                        "security-group-ids": "sg-0318560a006c62f73",
                        "security-groups": "launch-wizard-2",
                        "subnet-id": "subnet-0ea6276d365bee5bb",
                        "subnet-ipv4-cidr-block": "172.31.48.0/20",
                        "vpc-id": "vpc-0dc350ce2898d548a",
                        "vpc-ipv4-cidr-block": "172.31.0.0/16",
                        "vpc-ipv4-cidr-blocks": "172.31.0.0/16"
                    }
                }
            }
        },
        "placement": {
            "availability-zone": "us-east-1e",
            "availability-zone-id": "use1-az3",
            "region": "us-east-1"
        },
        "profile": "default-hvm",
        "public-hostname": "ec2-100-26-169-136.compute-1.amazonaws.com",
        "public-ipv4": "100.26.169.136",
        "reservation-id": "r-08a8ad4501c40b43c",
        "security-groups": "launch-wizard-2",
        "services": {
            "domain": "amazonaws.com",
            "partition": "aws"
        },
        "system": "xen"
    }
}`
